import React from 'react';
import ReactDOM from 'react-dom';

import {matrix_factory, identity_matrix_factory} from './matrix.js';

import './index.css';

const start_data = {matrix:[[.5,.25,.15],[.25,.4,.05],[.25,.35,.8]]
			};


ReactDOM.render(<App matrix={start_data.matrix}/>,
		document.getElementById("main-div"));   




//----------------------------------------
//
// expects:
// props.matrix
//
//----------------------------------------

function App(props) {
    const [matrix, set_matrix] = React.useState(props.matrix);

    const [init_state, set_init_state] = React.useState('B');
    const [cur_state, set_cur_state] = React.useState(init_state);

    const [iterations, set_iterations] = React.useState(1);

    const [show_ssv, set_show_ssv] = React.useState(false);

    // create a matrix to record prev states
    let tmp_ar = [[],[],[]];
    let tmp = letter_to_index(cur_state);
    tmp_ar[tmp].push(0);
    const [prev_state, set_prev_state] = React.useState(tmp_ar);

    function matrix_change_funct(i,j,value) {
	// if value is bigger than one, set the other editable entry
	// in that col to zero
	if(value > 1) {
	    value = 1;
	    if(i === 0) {
		set_matrix(prev_mat =>
			   set_entry_of_matrix(set_entry_of_matrix(prev_mat,1,j,0)),
			   2,j,0);
	    }
	    //else i = 1
	    else {
		set_matrix(prev_mat =>
			   set_entry_of_matrix(set_entry_of_matrix(prev_mat,0,j,0),2,j,0));
	    }
	}

	// if value is less than zero, set the other editable entry
	// in that col to one
	if(value < 0) {
	    value = 0;
	    if(i === 0) {
		set_matrix(prev_mat =>
			   set_entry_of_matrix(set_entry_of_matrix(prev_mat,1,j,1),
					       2,j,0));
	    }
	    //else i = 1
	    else {
		set_matrix(prev_mat =>
			   set_entry_of_matrix(set_entry_of_matrix(prev_mat,0,j,1),2,j,0));
	    }
	}

	//if i = 0
	if(i === 0) {
	    set_matrix(prev_mat =>
		       set_entry_of_matrix(prev_mat,0,j,value));
	    set_matrix(prev_mat => set_entry_of_matrix(prev_mat,2,j,1-value-prev_mat[1][j]));}
	//else i = 1
	else {
	    set_matrix(prev_mat =>
		   set_entry_of_matrix(prev_mat,1,j,value));
	    set_matrix(prev_mat => set_entry_of_matrix(prev_mat,2,j,1-value-prev_mat[0][j]));}
	set_iterations(1);
	//this is to make previous states start with 1 in correct place
	let tmp_ar = [[],[],[]];
	let tmp = letter_to_index(cur_state);
	tmp_ar[tmp].push(0);
	set_prev_state(tmp_ar);

	set_show_ssv(false);
    }

    function initial_change_funct(e) {
	let tmp_ar = [[],[],[]];
	set_init_state(e.target.value);

	if(e.target.value) {	    
	    set_cur_state(e.target.value);
	    
	    //this is to make previous states start with 1 in correct place
	    let tmp = letter_to_index(e.target.value);
	    tmp_ar[tmp].push(0);
	}

	
	set_iterations(1);	
	set_prev_state(tmp_ar);
    }

    function iterations_change_funct(e) {
	//set_iterations(e.target.value);
	let tmp_ar = [[],[],[]];
	set_prev_state(tmp_ar);

	let tmp_cur_state = cur_state;
	let ind;

	for(let i = 0; i<e.target.value; i++) {
	    tmp_cur_state = calc_new_state(tmp_cur_state);
	    ind = letter_to_index(tmp_cur_state);
	    tmp_ar  = tmp_ar.map((v,index) => {if(index === ind)
					       {return [...tmp_ar[index], i];}
					       else {return v;}});	 
	}

	set_prev_state(tmp_ar);
	set_cur_state(tmp_cur_state);
	set_iterations(e.target.value);
    }

    function next_state_funct() {	
	update_prev_state(cur_state);
	set_iterations(Number(iterations)+1);
	set_cur_state(calc_new_state(cur_state));	
    }

    function ssv_click_funct() {
	set_show_ssv(!(show_ssv));
    }

    function calc_new_state(cur) {
	
	// this makes the coordinate vector we need
	const i = letter_to_index(cur);
	const vec = Array(3).fill(0).map((v,index) => {if(index === i) {return 1;} else
					{return 0;}});
	
	const prob_vec = multiply_matrix_vec(matrix, vec);

	const rand = Math.random();

	if (rand < prob_vec[0]) {
	    return 'A';
	}

	else if (rand < prob_vec[0] + prob_vec[1]) {
	    return 'B';
	}

	return 'C';
    }

    function update_prev_state(cur) {	
	const i = letter_to_index(cur_state);
	const new_sub_ar = [...prev_state[i], iterations];

	const new_ar = prev_state.map((v,index) => {if(index === i)
						     {return new_sub_ar;}					
						    else {return v;}});
	set_prev_state(new_ar);	
    }
    
    return (<div className="input-div">
	    <div className="matrix-holder">	   
	    <ProbMatrix matrix={matrix}
	    on_change_funct={matrix_change_funct}/>
	     <input type="button" value ="Show steady state prob."
	    onClick={ssv_click_funct}/>
	    <SteadyStateVector show={show_ssv} matrix={matrix}/>	   
	    </div>
	    <hr/>
	    <div className="state-input">
	    <div className="state-left">
	    <div>
	    Initial state: <input type ="text" size="3"
	    value={init_state} onChange={initial_change_funct}/>
	    </div>
	    <div>
	    Current state:
	    <span>{cur_state}</span>&nbsp;&nbsp;&nbsp;&nbsp;
	    </div>
	    <input type="button" value ="Next state" onClick={next_state_funct}/>
	    </div>
	    <div className="state-right">
	    <span>
	    Iterations: <input type="text" size="6"
	    value={iterations} onChange={iterations_change_funct}></input>
	    </span>
	     <div>
 	    <table><tbody>
	    <tr>
 	    <td><div className="table-cell">state</div></td>
	    <td><div className="table-cell"></div></td>
	    <td>A</td><td>B</td><td>C</td>
	    </tr>
	    <tr>
	    <td>count</td>
	    <td><div className="table-cell"></div></td>
	    <td><div className="table-cell">{prev_state[0].length}</div></td>
	    <td><div className="table-cell">{prev_state[1].length}</div></td>
	    <td><div className="table-cell">{prev_state[2].length}</div></td>
	    </tr>
	    <tr>
 	    <td>%</td>
	    <td><div className="table-cell"></div></td>
	    <td>{round_twop(prev_state[0].length/(Number(iterations)))}</td>
	    <td>{round_twop(prev_state[1].length/(Number(iterations)))}</td>
	    <td>{round_twop(prev_state[2].length/(Number(iterations)))}</td>
 	    </tr>
 	    </tbody>
 	    </table>
 	    </div>
	    </div>
	    </div>
	    </div>
	   );
}



function letter_to_index(str) {
    if (str === 'A' || str === 'a') {
	return 0;
    }
	else if (str === 'B' || str === 'b') {
	    return 1;
	}
	else if (str === 'C' || str === 'c') {
	    return 2;
	}
}

// expects props.matrix; assumes it has eigenvalue 1
// calculates and displays unit eigenvector of eigenvalue 1
function SteadyStateVector(props) {   
    if(props.show) {
	//let m = Matrix.create(props.matrix);
	//let es = Matrix.eigenstructure(m).V.mat;
	let m = matrix_factory(props.matrix).add(identity_matrix_factory(3).scalar_multiply(-1));
	let ev = m.basis_null_space()[0];

	let sum = ev[0]+ev[1]+ev[2];
	let ssv = [ev[0]/sum, ev[1]/sum, ev[2]/sum];
 
	return <div className="vector-display">sspv = <table><tbody><tr>
	    <td>{round_twop(ssv[0])}</td></tr>
	    <tr><td>{round_twop(ssv[1])}</td></tr>
	    <tr><td>{round_twop(ssv[2])}</td></tr>
	    <tr></tr></tbody></table></div>;
    }

    return <div className="vector-display-phantom">sspv = <table><tbody><tr>
	    <td>0.00</td></tr>
	    <tr><td>0.00</td></tr>
	    <tr><td>0.00</td></tr>
	    <tr></tr></tbody></table></div>;
}



//----------------------------------------
//
// expects:
// props.matrix,
// props.change_function (this function expects arguments i and j)
//
//----------------------------------------
function ProbMatrix(props) {

    function make_table_cell(i,j) {
	return (<td><input type="test" size="6" value={(props.matrix[i])[j]}
		onChange={(e) => handle_change(e,i,j)}/></td>);
    }

    function handle_change(e,i,j) {
	props.on_change_funct(i,j,e.target.value);
    }

    function make_table_row(i) {
	let new_row = [0,1,2];

	return new_row.map(j => make_table_cell(i,j));
    }

    return (<table>
	    <tbody>
	    <tr><td></td><td>A&nbsp;&nbsp;</td><td>B&nbsp;&nbsp;</td><td>C&nbsp;&nbsp;</td>
	    </tr>
	    <tr>
	    <td>A&nbsp;</td>{make_table_row(0)}
	    </tr>
	    <tr>
	    <td>B&nbsp;</td>{make_table_row(1)}
	    </tr>
	    <tr>
	    <td>C&nbsp;</td>
	    <td>{round_twop(1-props.matrix[0][0]-props.matrix[1][0])}</td>
	    <td>{round_twop(1-props.matrix[0][1]-props.matrix[1][1])}</td>
	    <td>{round_twop(1-props.matrix[0][2]-props.matrix[1][2])}</td>
	    </tr>
	    </tbody>
	    </table>
	   );
}


//----------------------------------------
//
// set_entry_of_matrix
//
// input: matrix, array of arrays
//        i,j, value
//
//
// output: new matrix, array of arrays
//
//----------------------------------------
function set_entry_of_matrix(matrix, i,j,value) {
    let new_arr = [];
    for( let row = 0; row < matrix.length; row++) {
	new_arr[row]=[];
	for( let col = 0; col < matrix[row].length; col++) {
	    if(i === row && j === col) {new_arr[row][col]=value;}
	    else {new_arr[row][col]=matrix[row][col];}
	}
    }

    return new_arr;
}

function get_entry_of_matrix(matrix, i,j) {  
    return matrix[i][j];
}

//----------------------------------------
//
// multiply_matrix_vec
//
// input: matrix, array of arrays
//        vec, array of arrays
//
// output: vector, as array
//
//----------------------------------------
function multiply_matrix_vec(matrix, vec) {
    function add(reduction,element) {return reduction+element;}

    return matrix.map((i)=>(i.map((j,index)=>
				  j*vec[index]).reduce(add,0)));
}

function round_twop(num) {
    return (Math.round(100*num+.00001)/100);
}
